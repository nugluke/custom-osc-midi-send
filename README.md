# Custom OSC Midi Send

Patch de m4l baseado no [OSC MIDI SEND original do connection kit do Ableton](https://github.com/Ableton/m4l-connection-kit/blob/master/OSC%20MIDI%20Send/OSC%20MIDI%20Send.amxd)\
Nessa versão ele permite que o usuário adicione o OSC Adress para qual a nota midi ou a velocity vão ser enviadas.
 
 ![New Patch](https://gitlab.com/nugluke/custom-osc-midi-send/-/raw/master/assets/patch_screen.png "Patch")

